package com.exalt.springboot.canvas.service;

import java.util.Random;

public class PickCellService {
    private Integer height;
    private Integer width;
    private int index;
    private boolean bReGenRow;
    private boolean bReGenColumn;

    public PickCellService(Integer height, Integer width) {
        this.width = width;
        this.height = height;
        bReGenColumn = true;
        bReGenRow = true;
        pickCell();
    }

    public void pickCell() {
        index = new Random().nextInt(height*width);
        bReGenColumn = false;
        bReGenRow = false;
    }
    
    public int getCellRow() {
        if (bReGenColumn && bReGenRow) {
            pickCell();
        }
        bReGenRow = true;
        return index / width;
    }

    public int getCellColumn() {
        if (bReGenColumn && bReGenRow) {
            pickCell();
        }
        bReGenColumn = true;
        return index % width;
    }

    public int getCellIndex() {
        pickCell();
        return index;
    }
}
