package com.exalt.springboot.canvas.service;

import com.exalt.springboot.canvas.repository.GridRepository;

public class GridService {
    private GridRepository repository;

    public GridService(GridRepository repository) {
        this.repository = repository;
    }

    public Integer getGridHeight() {
        return repository.getGridHeight();
    }

    public void setGridHeight(Integer gridHeight) {
        repository.setGridHeight(gridHeight);
    }

    public Integer getGridWidth() {
        return repository.getGridWidth();
    }

    public void setGridWidth(Integer gridWidth) {
        repository.setGridWidth(gridWidth);
    }
    
    public String getColorAt(Integer row, Integer column) {
        try {
            return repository.getColorAt(row, column);
        } catch (Exception e) {
            System.out.println("Error when getting color");
            return null;
        }
    }

    public void setColorAt(Integer row, Integer column, String color) {
        try {
            repository.setColorAt(row, column, color);
        } catch (Exception e) {
            System.out.println("Error when setting color: " + color);
            return;
        }
    }
}
