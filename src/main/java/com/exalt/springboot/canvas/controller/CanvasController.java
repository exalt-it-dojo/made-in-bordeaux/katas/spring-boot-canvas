package com.exalt.springboot.canvas.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.exalt.springboot.canvas.model.ColoredCell;
import com.exalt.springboot.canvas.service.GridService;
import com.exalt.springboot.canvas.service.PickCellService;

import jakarta.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class CanvasController {
    private GridService gridService;
    private PickCellService pickCellService;

    public CanvasController(GridService gridService, PickCellService pickCellService) {
        this.gridService = gridService;
        this.pickCellService = pickCellService;
    }
    
    @GetMapping("/canvas")
    public ModelAndView getCanvas() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("gridHeight", gridService.getGridHeight());
        model.put("gridWidth", gridService.getGridWidth());
        return new ModelAndView("canvas", model);
    }

    @GetMapping("/pickcolor")
    public ModelAndView pickColor() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("gridHeight", gridService.getGridHeight());
        model.put("gridWidth", gridService.getGridWidth());
        int row = pickCellService.getCellRow();
        int column = pickCellService.getCellColumn();
        model.put("row", row);
        model.put("column", column);
        model.put("color", "");
        System.out.println("Selected row: " + row);
        System.out.println("Selected column: " + column);
        return new ModelAndView("canvas", model);
    }

    @PostMapping("/setcolor")
    public ModelAndView setColor(@Valid ColoredCell cell, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return new ModelAndView("canvas");
        }
        RedirectView redirectView = new RedirectView();
		redirectView.setUrl("/canvas");
		return new ModelAndView(redirectView);
    }
}
