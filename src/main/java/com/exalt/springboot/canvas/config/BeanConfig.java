package com.exalt.springboot.canvas.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.exalt.springboot.canvas.repository.GridRepository;
import com.exalt.springboot.canvas.service.GridService;
import com.exalt.springboot.canvas.service.PickCellService;

@Configuration
public class BeanConfig {

    @Bean
    public GridRepository gridRepository(){
        return new GridRepository(20, 40);
    }

    @Bean
    public GridService gridService() {
        return new GridService(gridRepository());
    }

    @Bean
    public PickCellService pickCellService() {
        return new PickCellService(20, 40);
    }
    
}
