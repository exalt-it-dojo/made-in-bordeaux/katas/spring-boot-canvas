package com.exalt.springboot.canvas.repository;

public class GridRepository {
    private Integer gridHeight;
    private Integer gridWidth;


    public GridRepository(Integer gridHeight, Integer gridWidth) {
        this.gridHeight = gridHeight;
        this.gridWidth = gridWidth;
    }

    public Integer getGridHeight() {
        return this.gridHeight;
    }

    public void setGridHeight(Integer gridHeight) {
        this.gridHeight = gridHeight;
    }

    public Integer getGridWidth() {
        return this.gridWidth;
    }

    public void setGridWidth(Integer gridWidth) {
        this.gridWidth = gridWidth;
    }
    
    public String getColorAt(Integer row, Integer column) throws IndexOutOfBoundsException {
        // TODO I am sure you can fix this one
        return "";
    }

    public void setColorAt(Integer row, Integer column, String color) throws IndexOutOfBoundsException {
        // TODO I am sure you can fix this one
    }
}
