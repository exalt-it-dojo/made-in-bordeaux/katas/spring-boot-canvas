package com.exalt.springboot.canvas.model;

import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotBlank;

public class ColoredCell {
    
    @Nonnull
    @NotBlank(message="Please pick a color")
    private String color;

    @Nonnull
    private Integer row;

    @Nonnull
    private Integer column;

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getRow() {
        return this.row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getColumn() {
        return this.column;
    }

    public void setColumn(Integer column) {
        this.column = column;
    }
}
