# Spring boot canvas

Ce kata test votre capacité à vous adapter à une code base existante.

Vous devrez comprendre la structure du code et trouver une bonne manière de livrer la solution au problème proposé.

## Canvas

"Canvas" signifie "toile" en français: lorsque vous lancez le programme pour la première fois, vous trouverez (à l'adresse http://localhost:8080/canvas) une grille de cases colorées.

Le but du jeu est de permettre au utilisateurs de proposer une couleur pour une case de la grille au hazard.

En cliquant sur le bouton "Pick color" vous verrez apparaître un composant qui vous permet de choisir une couleur.

La case à colorier est choisie aléatoirement pour chaque utilisateur.

L'utilisateur se contente de choisir une couleur pour la case selectionnée. Une fois la couleur choisie elle sera visible par tous les autres utilisateurs qui se connectent sur le "canvas".

Le but du jeu est de faire un dessin collectif. Pixel par pixel la foule essaie de faire un dessin.

## Votre travail

Le code fourni vous donne la grille et la structure générale du code mais la logique qui permet de colorier les cases est manquante.

Pouvez vous finir le travail ?

Vous devrez livrer votre code dans une merge request en portant une attention à la structure et à la lisibilité de votre code.

## Question bonus

Pouvez vous rendre configurable la taille de la grille ?

Il y a plusieurs approches possibles:
* configurer la taille de la grille par défaut avec des variables d'environnement
* faire en sorte que le premier utilisateur puisse choisir pour les autres

## How to run

### Running it from terminal

This project was set up using [gradle](https://gradle.org/). To run build it the first time just use the command:

```sh
gradle classes
```

Then, every time you wanna run it:

```sh
gradle bootRun
```

The application is listening on: http://localhost:8080/canvas

### Run it with visual studio

If you are using visual studio you can use this gradle plugin to run and debug.

```
Name: Gradle for Java
Id: vscjava.vscode-gradle
Description: Manage Gradle Projects, run Gradle tasks and provide better Gradle file authoring experience in VS Code
Version: 3.12.7
Publisher: Microsoft
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vscjava.vscode-gradle
```

The application is listening on: http://localhost:8080/canvas

### Run with maven

```sh
 ./mvnw spring-boot:run
```